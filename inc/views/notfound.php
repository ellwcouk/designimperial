<div class="container_24">
    <div class="whitebox">
        <div class="grid_22">
            <h1>Not found</h1>
            <p>Sorry, but the page you were looking is not found. Please click <a href="<?php echo $this->baseUrl('index'); ?>" title="home">here</a> to go back to the main page.</p>
            <p></p>
        </div></div></div>
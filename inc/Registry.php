<?php

class Registry
{
    
    protected static $_registry = array();
    
    public static function get($name)
    {
        return self::$_registry[$name];
    }
    
    public static function set($name, $value)
    {
        self::$_registry[$name] = $value;
        return;
    }
}
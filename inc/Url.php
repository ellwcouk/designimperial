<?php
class Url
{
    
    public function getUrl()
    {
        $url = $this->_cleanUrl($_SERVER['REQUEST_URI']);
        
        $parts = explode('/', $url);
        
        $uri = array();
        
        if(!isset($parts[0]) || empty($parts[0]))
        {
            $uri[] = 'index';
        } else {
            $uri[] = $parts[0];
        }
        unset($parts[0]);
        
        $keyHolder = '';
        $i = 0;
        foreach($parts as $value)
        {
            if($i == 0)
            {
                $keyHolder = $value;
                $i++;
            } else {
                $uri[$keyHolder] = $value;
                $i--;
            }
        }
        
        return $uri;
    }
    
    protected function _cleanUrl($url)
    {
        // Remove the base url
        $url = str_replace(Helpers::baseUrl(), '/', $url);
        $url = strtolower(preg_replace('/[^a-zA-Z0-9\/]/', '', $url));
        if(substr($url, 0, 1) == '/')
        {
            $url = substr($url, 1);
        }  
        
        return $url;
    }
}
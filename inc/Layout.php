<?php

class Layout
{
    
    public static $instance;
    
    public $viewScript;
    public $viewName;
    public $layoutScript;
    public $layoutName;
    
    public static function getInstance()
    {
        if(!self::$instance)
        {
            self::$instance = new Layout;
        }
        
        return self::$instance;
    }
    
    public function __set($name, $value)
    {
        $this->{$name} = $value;
    }
    
    public function __get($name)
    {
        if(isset($name))
        {
            return $name;
        }
        
        return false;
    }
    
    public function setViewName($viewName)
    {
        $this->viewName = $viewName;
        return self::$instance;
    }
    
    public function content()
    {
        return $this->viewScript;
    }
    
    public function setLayoutName($layoutName)
    {
        $this->layoutName = $layoutName;
        return self::$instance;
    }
    
    public function run()
    {
        // get the views...
        ob_start();
        include BASEDIR . '/inc/views/' . $this->viewName . '.php';
        $this->viewScript = ob_get_contents();
        ob_end_clean();
        // get the layout...
        ob_start();
        include BASEDIR . '/inc/layouts/default.php';
        $this->layoutScript = ob_get_contents();
        ob_end_clean();
        // and return it all in a nice variable!
        return $this->layoutScript;
    }
    
    public function baseUrl($url = '')
    {
        return Helpers::baseUrl($url);
    }
}
<?php
error_reporting(-1);
ini_set("display_errors", "On");
ini_set("display_startup_errors", "On");
class Website 
{
    
    public function __construct()
    {
        spl_autoload_register(array($this, 'autoloader'));
    }
    
    public function __set($name, $value)
    {
        $this->{$name} = $value;
    }
    
    public function __get($name)
    {
        if(isset($name))
        {
            return $name;
        }
        
        return false;
    }
    
    public function autoloader($className)
    {
        if(class_exists($className, false) || interface_exists($className, false))
        {
            return;
        }
        
        $fileName = ucfirst($className) . '.php';
        
        if(file_exists(BASEDIR . '/inc/' . $fileName))
        {
            include BASEDIR . '/inc/' . $fileName;
            return;
        }
        
        throw new Exception('Class not found: ' .$className);
    }
    
    public function run()
    {
        $url = new Url();
        $uri = $url->getUrl();
        
        Registry::set('uri', $uri);
        
        $layout = Layout::getInstance();
        if($this->viewExists($uri[0]))
        {
            $layout->setViewName($uri[0]);
        } else {
            $layout->setViewName('notfound');
        }
        
        echo $layout->run();
    }
    
    public function viewExists($view = 'index')
    {
        if(file_exists(BASEDIR . '/inc/views/' . $view . '.php'))
        {
            return true;
        }
        return false;
    }
}

date_default_timezone_set('Europe/London');
if(!defined('BASEDIR'))
{
    define('BASEDIR', realpath(dirname(__FILE__)));
}

if(!defined('ENV'))
{
    define('ENV', 'development');
}
$website = new Website;
$website->run();
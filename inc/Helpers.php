<?php

class Helpers
{
    
    public static function baseUrl($url = '')
    {
        return substr($_SERVER['PHP_SELF'], 0, -9) . $url;
    }
}
<!DOCTYPE html>
<html lang="en" class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?php echo $this->title; ?></title>
        <meta name="description" content="<?php echo $this->description; ?>">
        <link rel="stylesheet" media="screen" href="<?php echo $this->baseUrl('css/style.css'); ?>">
    </head>
    <body>
        <div id="ribbon">&nbsp;</div>
        <!--[if lt IE 7]>
        <p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
    <![endif]-->
        <div id="topbar">
            <div class="wrapper lightgrey font_12">
                <div class="left">
                    SOCIAL MEDIA
                </div>
                <div class="right margintop_10">
                    <img src="<?php echo $this->baseUrl('img/rss.png'); ?>" alt="RSS feed" /> +44 (0) 1900 837 912
                </div>
            </div> 
        </div>
        <div class="clear"></div>
        <a name="top"></a>
        <div class="wrapper" id="container">
            <div id="header">
                <div id="logo" class="left">
                    <h1 class="ir">DesignImperial</h1><h2 class="ir">Dedicated to your online success</h2>
                    
                </div>
                <div class="right" id="headerphonenumber">
                    <img src="img/phone.png" alt="Telephone Number of Design Imperial" /> +44 (0) 1900 837 912<br />
                    EMAIL ICON info@designimperial.com
                </div>
            </div>
            <div class="clear"></div>
            <div id="testimonials">
                <ul id="textslider_slide">
                    <li>"A pleasure to work with, Design Imperial gave us an incredible site and brought my dead pets back to life!" - <span>Jack Johnson, CEO of MagicBeans</span></li>
                    <li>"A pleasure to work with, DesignImperial gave us an incredible site and brought my dead pets back to life!" - <span>Jack Johnson, CEO of MagicBeans</span></li>
                    <li>"A pleasure to work with, DesignImperial gave us an incredible site and brought my dead pets back to life!" - <span>Jack Johnson, CEO of MagicBeans</span></li>
                    <li>Fourth Item</li>
                    <li>Fifth Item</li>
                </ul>

            </div>
            <div class="clear"></div>
            <ul id="menu">
                <li><a href="<?php echo $this->baseUrl('index'); ?>">Home</a></li>
                <li><a href="<?php echo $this->baseUrl('web-design'); ?>">Web Design</a></li>
                <li><a href="<?php echo $this->baseUrl('web-optimization'); ?>">Web Optimization</a></li>
                <li>
                    <a href="<?php echo $this->baseUrl('web-solutions'); ?>">Web Solutions</a>
                    <ul>
                        <li><a href="<?php echo $this->baseUrl('web-solutions/ecommerce'); ?>">eCommerce</a></li>
                        <li><a href="<?php echo $this->baseUrl('web-solutions/database-systems'); ?>">Database Systems</a></li>
                        <li><a href="<?php echo $this->baseUrl('web-solutions/custom-solutions'); ?>">Custom Solution</a></li>
                    </ul>
                </li>
                <li><a href="<?php echo $this->baseUrl('portfolio'); ?>">Our Portfolio</a></li>
                <li><a href="<?php echo $this->baseUrl('contact-us'); ?>">Contact Us</a></li>
            </ul>
            <div class="clear"></div>
            <?php echo $this->content(); ?>
            <div id="footer">
                <div id="topfooter">
                    <div class="left">
                        //  <a href="#" title="#">Web Design</a> //  Social Marketing and SEO  //  Our Portfolio //  Our Blog  // Moar Stuff // Contact Us
                    </div>
                    <div class="right">
                        <a href="#top" title="Back to Top of page">Back to top <img src="<?php echo $this->baseUrl('img/uparrow.png'); ?>" alt="Back to Top" /></a>
                    </div>
                </div>
                <div id="bottomfooter">
                    <div class="col_320">
                        <h3>About Design Imperial</h3><hr />
                        <p>We’re a group of highly intelligent, talent webdesigners who are passionate about the world of web.</p>

                        <p>We can honestly guarantee that you will not higher quality websites at a lower price. We’re tired of seeing people taking advantage of the little guy, with leading companies charging  
Ishit loads of monies for mundane work. Silly sausages that they are! Silly sausages that they are!Silly sausages that they are!Silly sausages that they are!Silly sausages that they are!Silly sausages that they are!Silly sausages that they are!Silly sausages that they are!</p>
                    </div>
                    <div class="col_320">
                        <h3>Frequently Asked Questions</h3>
                        <hr />
                        How long will it take to design and develop my Website?
                        <hr />
                        How long will it take to design and develop my Website?
                        <hr />
                        How long will it take to design and develop my Website?
                        <hr />
                    </div>
                    <div class="col_320">
                        <h3>Tweets</h3><hr />
                        <!--<div class="tweet"></div>-->
                        <div id="tweets">
                            <div class="tweet">
                                <a href="#">@Design Imperial</a> we said hello to the world whoooooo!
                                <div class="datetime">6 days ago</div>
                            </div>
                        </div>
                        <div id="twitterfooter">
                            <a href="http://twitter.com/#!/design_imperial" target="_blank"><img src="img/twitter-footer.png" alt="" /> Follow us on Twitter</a>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
                &copy; Design Imperial <?php echo date('Y'); ?>
            </div>
        </div>
        <br />
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
        <script src="<?php echo $this->baseUrl('js/script.js'); ?>"></script>
        <script src="<?php echo $this->baseUrl('js/textslider.js'); ?>"></script>
        <script src="<?php echo $this->baseUrl('js/lazyload.js'); ?>"></script>
        <script>
            $(function() {
                $("img").lazyload({
                    effect : "fadeIn",
                    
                    appear : function(elements_left, settings) {
                        console.log("appear");
                        console.log(elements_left);
                        console.log(this, elements_left, settings);
                    },
                    load : function(elements_left, settings) {
                        console.log("load");
                        console.log(elements_left);
                        console.log(this, elements_left, settings);
                    }
                    
                });
                
                $('#textslider_slide').list_ticker({
                    speed:5000,
                    effect:'slide'
		});
                
                //$('#ribbon').jScroll();
                $('#ribbon').mouseover(function() {
                    $(this).stop().animate({
                        marginLeft: '48px'
                    });
                }).mouseout(function() {
                    $(this).stop().animate({
                        marginLeft: '0px'
                    });
                }).click(function() {
                    window.location.replace('localhost/designimperial/portfolio');
                });
                
                $('a[href=#top]').click(function(){
                    $('html, body').animate({scrollTop:0}, 'slow');
                    return false;
                });
            });
            

            
            
            var fadeDuration=2000;
            window.slideDuration=4000;
            var currentIndex=1;
            var nextIndex=1;
            $(document).ready(function()
            {
                $('ul.slideshow li').css('left', '30px');
		$('ul.slideshow li').css({opacity: 0.0});
		$("'ul.slideshow li:nth-child("+nextIndex+")'").addClass('show').animate({opacity: 1.0}, fadeDuration);
		window.timer = setInterval('nextSlide()',window.slideDuration);
                
                // image clicker
                $('#slideshowoverimg a').click(function(event) {
                    event.preventDefault();
                    event.stopPropagation();
                    var rel = $(this).attr("rel");
                    clickerSlide(rel);
                });
            });
            function nextSlide(){
                nextIndex =currentIndex+1;
                if(nextIndex > $('ul.slideshow li').length)
                {
                    nextIndex =1;
                }
                $("'ul.slideshow li:nth-child("+nextIndex+")'").addClass('show').stop().animate({opacity: 1.0}, fadeDuration);
                $("'ul.slideshow li:nth-child("+currentIndex+")'").stop().animate({opacity: 0.0}, fadeDuration).removeClass('show');
                var title = $("'ul.slideshow li:nth-child("+nextIndex+")'").find('a').attr('title');
                var text = $("'ul.slideshow li:nth-child("+nextIndex+")'").find('img').attr('alt');
                $('#slideshowtext').html('<h3>' + title + '</h3><p>' + text + '</p>').stop().hide().fadeIn(2000);
                currentIndex = nextIndex;
            }
            function clickerSlide(index) {
                clearInterval(window.timer);
                $("'ul.slideshow li:nth-child("+index+")'").addClass('show').clearQueue().stop().animate({opacity: 1.0}, fadeDuration);
                $("'ul.slideshow li:nth-child("+currentIndex+")'").stop().animate({opacity: 0.0}, fadeDuration).removeClass('show');
                var title = $("'ul.slideshow li:nth-child("+index+")'").find('a').attr('title');
                var text = $("'ul.slideshow li:nth-child("+index+")'").find('img').attr('alt');
                $('#slideshowtext').html('<h3>' + title + '</h3><p>' + text + '</p>').stop().hide().fadeIn(2000);
                currentIndex = index+1;
                window.timer = setInterval('nextSlide()',window.slideDuration);
            }
            
            var searchval = 'Search our stuff';
            $('#search').val(searchval);
            $('#search').on("focus", function() {
                if($(this).val() == searchval) {
                    $(this).val('');
                }
            }).on("blur", function() {
                if($(this).val() == '') {
                    $(this).val(searchval);
                }
            });
            
                 $(function() {
                $('#tweets').html('Loading Tweets...')
                var output = '';
                $.getJSON('http://twitter.com/statuses/user_timeline.json?screen_name=design_imperial&count=3&callback=?', function(data)      {
                    $.each(data, function(key, value)
                    {
                        var tweet = value.text;
                        console.log(value);
                        tweet = tweet.replace(/(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig, function(url) {
                            return '<a href="'+url+'">'+url+'</a>';
                        }).replace(/B@([_a-z0-9]+)/ig, function(reply) {
                            return  reply.charAt(0)+'<a href="http://twitter.com/'+reply.substring(1)+'">'+reply.substring(1)+'</a>';
                        });
                        var link = '<a href="https://twitter.com/design_imperial/status/'+value.id_str+'" title="View our Tweets" target="_blank">';
                        var prepend = '<a href="http://twitter.com/design_imperial" title="Follow us on Twitter" target="_blank">@Design Imperial</a> ';
                        var date = TwitterDateConverter(new Date(value.created_at));
                        
                        var start = '<div class="tweet">' + prepend + tweet + link + 'View Tweet</a>' + '<div class="datetime">' + date + '</div></div>';
                        //if(output == '') {
                           // output = start
                       // } else {
                            output = output + start;
                      //  }
                        
                        
                    });
                    $('#tweets').html(output);
                }); 
                
                function TwitterDateConverter(time){
                    var date = new Date(time),
                    diff = (((new Date()).getTime() - date.getTime()) / 1000),
                    day_diff = Math.floor(diff / 86400);
                    
                    if ( isNaN(day_diff) || day_diff < 0 || day_diff >= 31 )
                        return;
                    
                    return day_diff == 0 && (
                    diff < 60 && "just now" ||
                        diff < 120 && "1 minute ago" ||
                        diff < 3600 && Math.floor( diff / 60 ) + " minutes ago" ||
                        diff < 7200 && "1 hour ago" ||
                        diff < 86400 && Math.floor( diff / 3600 ) + " hours ago") ||
                        day_diff == 1 && "Yesterday" ||
                        day_diff < 7 && day_diff + " days ago" ||
                        day_diff < 31 && Math.ceil( day_diff / 7 ) + " weeks ago";
                }
                
            });
            
        </script>
    </body>
</html>